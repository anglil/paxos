
public class Result {
	public final int clientID;
	public final int clientSeq;
	public final int result;
	
	
	public Result(int clientID, int clientSeq, int result) {
		this.clientID = clientID;
		this.clientSeq = clientSeq;
		this.result = result;
	}
}
