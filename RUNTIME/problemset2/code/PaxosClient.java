import java.util.HashMap;


public class PaxosClient {
	private final int sequenceNumberGap = 100;
	private final int id;
	private final int slotID;
	private final MessagePassingInterface mpi;
	private final HashMap<Integer, ServerAddress> servers;
	private final ServerAddress myAddress;
	
	
	private int proposeSeq;
	private int prepareReplyCount;
	private int highSeq;
	private Command command;
	private Command highSeqCommand;
	
	
	public PaxosClient (int id, int slotID, MessagePassingInterface mpi,
			HashMap<Integer, ServerAddress> servers, ServerAddress myAddress) {
		this.id = id;
		this.slotID = slotID;
		this.mpi = mpi;
		this.servers = servers;
		this.myAddress = myAddress;
		this.proposeSeq = id + this.sequenceNumberGap;
	}
	
	public void handleMessage(Message message) {
		if (message.messageType == MessageType.PAXOSPREPAREREPLY) {
			handlePaxosPrepareReply(message);
		}
	}
	
	
	public void restart(Command command) {
		this.command = command;
		this.prepareReplyCount = 0;
		this.highSeq = -1;
		this.highSeqCommand = null;
		for (ServerAddress server: this.servers.values()) {
			Message m = new Message(this.slotID, this.id, command.clientSeq, this.myAddress, 
					MessageType.PAXOSPREPARE, null, this.proposeSeq);
			this.mpi.sendMessage(server, m);
		}
		this.proposeSeq += sequenceNumberGap;
	}
	
	
	private void handlePaxosPrepareReply(Message message) {
		if (message.result > this.highSeq) {
			this.highSeq = message.result;
			this.highSeqCommand = message.command;
		}
		this.prepareReplyCount++;
		if (this.prepareReplyCount >=2 ) {
			if (this.highSeqCommand == null) {
				this.highSeqCommand = this.command;
			}
			for (ServerAddress server: this.servers.values()) {
				Message m = new Message(this.slotID, this.id, message.clientSeq, this.myAddress, 
						MessageType.PAXOSACCEPT, this.highSeqCommand, this.proposeSeq);
				this.mpi.sendMessage(server, m);
			}
		}
	}
}
