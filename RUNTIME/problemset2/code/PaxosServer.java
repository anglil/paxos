import java.util.HashMap;


public class PaxosServer {
	private final Server server;
	public final int id;
	public final int slotID;
	private final MessagePassingInterface mpi;
	private final HashMap<Integer, ServerAddress> servers;
	private final ServerAddress myAddress;
	private int highestProposalNumber;
	private int sequence_number;
	private Command command;
	
	private final HashMap<Command, Integer> accepted;
	
	public PaxosServer(Server server, int id, int slotID, MessagePassingInterface mpi,
			HashMap<Integer, ServerAddress> servers, ServerAddress myAddress) {
		this.server = server;
		this.id = id;
		this.slotID = slotID;
		this.mpi = mpi;
		this.servers = servers;
		this.myAddress = myAddress;
		this.highestProposalNumber = 0;
		this.sequence_number = 0;
		this.command = null;
		this.accepted = new HashMap<Command, Integer>();
	}
	
	public void handleMessage(Message message) {
		if (message.messageType == MessageType.PAXOSPREPARE) {
			handlePaxosPreare(message);
		}
		if (message.messageType == MessageType.PAXOSACCEPT) {
			handlePaxosAccept(message);
		}
		if (message.messageType == MessageType.PAXOSACCEPTREPLY) {
			handlePaxosAcceptReply(message);
		}
	}

	private void handlePaxosPreare(Message message) {
		if (message.result < this.highestProposalNumber) {
			return;
		}
		this.highestProposalNumber = message.result;
		Message m = new Message(this.slotID, message.clientID,message.clientSeq, this.myAddress, 
				MessageType.PAXOSPREPAREREPLY, this.command, this.sequence_number);
		this.mpi.sendMessage(message.sender, m);
	}
	
	
	private void handlePaxosAccept(Message message) {
		if (message.command == null) {
			System.out.println("BUG!");
			System.exit(-1);
		}
		
		if (message.result < this.highestProposalNumber) {
			return;
		}
		this.highestProposalNumber = message.result;
		this.sequence_number = message.result;
		this.command = message.command;
		for (ServerAddress server: this.servers.values()) {
			Message m = new Message(this.slotID, this.id,  message.clientSeq, this.myAddress, 
					MessageType.PAXOSACCEPTREPLY, this.command, this.sequence_number);
			this.mpi.sendMessage(server, m);
		}
	}
	
	private void handlePaxosAcceptReply(Message message) {
		if (message.command == null) {
			System.out.println("BUG!");
			System.exit(-1);
		}
		
		for (Command c : this.accepted.keySet()) {
			if (c.compareTo(message.command) == 0) {
				if (this.accepted.get(c) != message.clientID) {
					notifyServer(c);
				}
				return;
			}
		}
		
		this.accepted.put(message.command, message.clientID);
		
	}

	private void notifyServer(Command c) {
		Logger.printDebug("notify server!");
		server.newCommitedTask(slotID, c);
	}
	
}
