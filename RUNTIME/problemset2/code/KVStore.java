import java.util.HashMap;


public class KVStore {
	
	private final HashMap<String, Value> store; // the entity of the key-value store
	
	public KVStore() {
		this.store = new HashMap<String, Value>(); // a public key-value store
	}
	
	public Integer read(String variable) {
		if (this.store.get(variable) == null) {
			this.store.put(variable, new Value(0,false,-1));
		}
		return this.store.get(variable).value;
	}
	
	public boolean write(int clientID, String variable, int value) {
		if (this.store.get(variable) != null) {
			if (this.store.get(variable).lock == true) {
				if (this.store.get(variable).owner == clientID) {
					this.store.put(variable, new Value(value,true,
							this.store.get(variable).owner));
					return true;
				}
				return false;
			}
		}
		this.store.put(variable, new Value(value,false, -1));
		return true;
	}
	
	public boolean lock(int clientID, String variable) {
		Logger.printDebug("ClientID = " + clientID + " Locks the System");
		if (this.store.get(variable) == null) {
			this.store.put(variable, new Value(0,false, -1));
		}
		if (this.store.get(variable).lock == true) {
			if (this.store.get(variable).owner == clientID) {
				return true;
			}
			return false;
		}
		this.store.get(variable).lock = true;
		this.store.get(variable).owner = clientID;
		return true;
	}
	
	public void unlock(String variable) {
		if (this.store.get(variable) == null) {
			this.store.put(variable, new Value(0,false, -1));
		}
		
		this.store.get(variable).lock = false;
		this.store.get(variable).owner = -1;
	}
	
	private class Value {
		int value;
		boolean lock; 
		int owner;
		
		public Value(int value, boolean lock, int owner) {
			this.value = value;
			this.lock = lock;
			this.owner = owner;
		}
	}
}
