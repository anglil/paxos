import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;


public class Client implements Runnable{
	private final int id;
	private final MPIImpl mpi;
	private final HashMap<Integer, ServerAddress> servers;
	private final ServerAddress myAddress;
	
	/***
	 * Constructor of a Client
	 * @param id							ClientID, this must be unique across all the clients
	 * @param mpi							MessagePassingInterface
	 * @param servers						Address of the LockService
	 * @throws UnknownHostException
	 */
	public Client(int id, MPIImpl mpi, HashMap<Integer, ServerAddress> servers) throws UnknownHostException {
		this.id = id;
		this.mpi = mpi;
		this.servers = servers;
		myAddress = new ServerAddress(Inet4Address.getLocalHost().getHostAddress(), mpi.port);
	}
	
	/***
	 * Implements a User Level Terminal for inputs
	 */
	@Override
	public void run() {
		try {
			Scanner sc = new Scanner(System.in);
			int clientSeq = 0;
			while (true) {
				System.out.println("Please Enter Your Commands:");
				String line = sc.nextLine();
				if (line.contentEquals("QUIT")) {
					break;
				}
			
				if (line.startsWith("READ")) {
					if (line.split(" ").length != 2) {
						System.out.println("Usage: READ <VariableName>");
						continue;
					}
					String variableName = line.split(" ")[1];
					int value = read(clientSeq, variableName);
					System.out.println(variableName + "=" + value);
				}
			
				if (line.startsWith("WRITE")) {
					if (line.split(" ").length != 3) {
						System.out.println("Usage: WRITE <VariableName> <value>");
						continue;
					}
					String variableName = line.split(" ")[1];
					int value = Integer.parseInt(line.split(" ")[2]);
					write(clientSeq, variableName, value);
				}
			
				if (line.startsWith("LOCK")) {
					if (line.split(" ").length != 2) {
						System.out.println("Usage: LOCK <VariableName>");
						continue;
					}
					String variableName = line.split(" ")[1];
					lock(clientSeq, variableName);
				}
			
				if (line.startsWith("UNLOCK")) {
					if (line.split(" ").length != 2) {
						System.out.println("Usage: UNLOCK <VariableName>");
						continue;
					}
					String variableName = line.split(" ")[1];
					unlock(clientSeq, variableName);
				}
				clientSeq ++;
				Logger.printDebug("Command Completes on the Server!");
			}
			sc.close();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			this.mpi.cleanUp();
			System.out.println("Some RunTime Error");
		}
	}

	
	private int getSlotID() {
		while (true) {
			Logger.printDebug("Try to get a new SlotID");
			for (ServerAddress server : this.servers.values()) {
				Message message = new Message(0, this.id, 0, this.myAddress, MessageType.REQUESTSLOTID,
						null, 0);
				mpi.sendMessage(server, message);
			}
			int count = 0;
			int highestSlotID = -1;
			boolean flag = false;
			while (count < 2) {
				Message message = mpi.getMessage(true);
				if (message == null) {
					flag = true;
					break;
				}
				if (message.messageType != MessageType.REQUESTSLOTIDREPLY) {
					continue;
				}
				if (message.slotID > highestSlotID) {
					highestSlotID = message.slotID;
				}
				count ++;
			}
			if (flag) {
				Logger.printDebug("No SlotID info!");
				continue;
			}
			Logger.printDebug("get new Slot ID:" +  highestSlotID);
			return highestSlotID;
		}
	}
	

	private int read(int clientSeq, String variable) {
		while (true) {
			int slotID = getSlotID();
			Command command = new Command(slotID, myAddress, this.id, clientSeq, CommandType.READ,
					variable, 0);
			PaxosClient pc = new PaxosClient(this.id, slotID, mpi, servers, myAddress);
			pc.restart(command);
			while (true) {
				Message m = this.mpi.getMessage(true);
				if (m==null) {
					break;
				}
				if (m.clientSeq != clientSeq) {
					continue;
				}
				if (m.messageType == MessageType.READRESULT) {
					if (m.command.clientSeq == clientSeq) {
						return m.result;
					}
				}
				pc.handleMessage(m);
			}
		}
	}
	
	private void write(int clientSeq, String variable, int value) throws InterruptedException {
		while (true) {
			int slotID = getSlotID();
			Command command = new Command(slotID, myAddress, this.id, clientSeq, CommandType.WRITE,
					variable, value);
			PaxosClient pc = new PaxosClient(this.id, slotID, mpi, servers, myAddress);
			pc.restart(command);
			while (true) {
				Message m = this.mpi.getMessage(true);
				if (m==null) {
					Random rand = new Random();
					Thread.sleep((long) (500 + rand.nextDouble()*1000));
					break;
				}
				if (m.clientSeq != clientSeq) {
					continue;
				}
				if (m.messageType == MessageType.WRITERESULT) {
					if (m.command.clientSeq == clientSeq) {
						if (m.result == 1) {
							return;
						} else {
							Random rand = new Random();
							Thread.sleep((long) (500 + rand.nextDouble()*1000));
							break;
						}
					}
				}
				pc.handleMessage(m);
			}
		}
	}
	
	private void lock(int clientSeq, String variable) throws InterruptedException {
		while (true) {
			int slotID = getSlotID();
			Logger.printDebug("I try to lock with slotID = " + slotID);
			Command command = new Command(slotID, myAddress, this.id, clientSeq, CommandType.LOCK,
					variable, 0);
			PaxosClient pc = new PaxosClient(this.id, slotID, mpi, servers, myAddress);
			pc.restart(command);
			while (true) {
				Message m = this.mpi.getMessage(true);
				if (m==null) {
					Random rand = new Random();
					Thread.sleep((long) (500 + rand.nextDouble()*1000));
					break;
				}
				if (m.clientSeq != clientSeq) {
					continue;
				}
				if (m.messageType == MessageType.LOCKRESULT) {
					if (m.command.clientSeq == clientSeq) {
						if (m.result == 1) {
							return;
						}
						Random rand = new Random();
						Thread.sleep((long) (500 + rand.nextDouble()*1000));
						break;
					}
				}
				pc.handleMessage(m);
			}
		}
	}
	
	private void unlock(int clientSeq, String variable) {
		while (true) {
			int slotID = getSlotID();
			Command command = new Command(slotID, myAddress, this.id, clientSeq, CommandType.UNLOCK,
					variable, 0);
			PaxosClient pc = new PaxosClient(this.id, slotID, mpi, servers, myAddress);
			pc.restart(command);
			while (true) {
				Message m = this.mpi.getMessage(true);
				if (m==null) {
					break;
				}
				if (m.clientSeq != clientSeq) {
					continue;
				}
				if (m.messageType == MessageType.UNLOCKRESULT) {
					if (m.command.clientSeq == clientSeq) {
						return;
					}
					break;
				}
				pc.handleMessage(m);
			}
		}
	}
}
