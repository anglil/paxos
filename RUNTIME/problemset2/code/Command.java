import java.io.Serializable;


public class Command implements Serializable, Comparable<Command>{
	private static final long serialVersionUID = 1L;
	public final CommandType commandType;
	public final ServerAddress commanderAddr;
	public final int clientID;
	public final int clientSeq;
	public final int slotID;
	public final String variable;
	public final int value;
	
	public Command(int slotID, ServerAddress commanderAddr, int clientID, int clientSeq,
			CommandType commandType, String variable, int value) {
		this.slotID = slotID;
		this.commanderAddr = commanderAddr;
		this.clientID = clientID;
		this.clientSeq = clientSeq;
		this.commandType = commandType;
		this.variable = variable;
		this.value = value;
	}

	@Override
	public int compareTo(Command e) {
		
		if ((commandType == CommandType.NOP) && (e.commandType == CommandType.NOP)) {
			return 0;
		}
		
		if (commandType != e.commandType)
			return 1;
		if (clientID != e.clientID)
			return 1;
		if (clientSeq != e.clientSeq)
			return 1;
		if (slotID != e.slotID)
			return 1;
		return 0;
	}
}
