import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

// MessagePassingInterface is an interface
public class MPIImpl implements MessagePassingInterface {
	private final ServerSocket serverSocket; // a server socket that uses TCP
	public final int port;
	public MPIImpl(int port) throws IOException {
		this.serverSocket = new ServerSocket(port);
		this.port = port;
	}
	
	@Override
	public Message getMessage(boolean timeout) {
		while (true) {
			try {
				if (timeout) {
					this.serverSocket.setSoTimeout(100);
				} else {
					this.serverSocket.setSoTimeout(0);
				}
				Socket clientSocket = this.serverSocket.accept(); // Java encapsulates socket operations into a socket class
				ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream()); // get the input stream from the client socket
				Message message = (Message) in.readObject(); // read the message from socket, and convert from object to message type
				in.close();
				clientSocket.close();
				return message;
			} catch (ClassNotFoundException e) {
			} catch (IOException e) {
				return null;
			}
		}
	}
	
	@Override
	public void sendMessage(ServerAddress address, Message message) {
//		Random rand = new Random();
//		if (rand.nextDouble()<0.05) {
//			return;
//		}
		try {
			Socket socket = new Socket(address.hostName, address.port); // java.net.Socket
			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			out.writeObject(message); // write the message into the socket to send
			out.close();
			socket.close();
		} catch (UnknownHostException e) {
		} catch (IOException e) {
		} finally {
			
		}
	}

	@Override
	public void cleanUp() {
		try {
			this.serverSocket.close(); // close the private serverSocket
		} catch (IOException e) {
		}
	}
}
