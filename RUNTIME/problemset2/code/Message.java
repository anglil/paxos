import java.io.Serializable;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;
	public final int clientID;
	public final int clientSeq;
	public final ServerAddress sender;
	public final MessageType messageType;
	public final Command command;
	public final int result;
	public final int slotID;
	
	
	/***
	 * Original Constructor for a Message
	 * @param slotId
	 * @param clientID
	 * @param sender
	 * @param messageType
	 * @param command
	 * @param result
	 */
	public Message(int slotID, int clientID, int clientSeq, ServerAddress sender, MessageType messageType,
			Command command, int result) {
		this.slotID = slotID;
		this.clientID = clientID;
		this.clientSeq = clientSeq;
		this.sender = sender;
		this.messageType = messageType;
		this.command = command;
		this.result = result;
	}
}