import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;


public class ConfigurationParser {
	public static HashMap<Integer,ServerAddress> getConfiguration() throws FileNotFoundException {
		HashMap<Integer, ServerAddress> servers = new HashMap<Integer, ServerAddress>();
		Scanner sc = new Scanner(new FileInputStream("Configuration.txt"));
		while (sc.hasNextLine()) {
			String line = sc.nextLine(); // get the next line from the scanner
			String[] serverInfo = line.split(" "); // split by a space
			int serverID = Integer.parseInt(serverInfo[0]); // a function that belongs to class Integer
			String serverHostName = serverInfo[1]; 
			int serverPort = Integer.parseInt(serverInfo[2]);
			servers.put(serverID, new ServerAddress(serverHostName, serverPort));
		}
		sc.close();
		return servers;
	}
}
