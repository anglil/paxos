import java.io.IOException;
import java.util.HashMap;


public class Main {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		if (args.length < 2) {
			printUsage();
		}
		
		// check whether the input contains the correct server or client name 
		boolean isServer = false;
		if (args[0].contentEquals("SERVER")) {
			isServer = true;
		} else if (args[0].contentEquals("CLIENT")) {
			isServer = false;
		} else {
			printUsage();
		}
		
		HashMap<Integer, ServerAddress> servers = ConfigurationParser.getConfiguration();
		MPIImpl mpi = null;
		if (!isServer) {
			if (args.length != 3) {
				printUsage();
			}
			int id = Integer.parseInt(args[1]);
			mpi = new MPIImpl(Integer.parseInt(args[2])); // get the command and construct the message interface
			Client client = new Client(id, mpi, servers);
			client.run();
		} else {
			if (args.length != 2) {
				printUsage();
			}
			int id = Integer.parseInt(args[1]); // get the command
			mpi = new MPIImpl(servers.get(id).port);
			Server server = new Server(id, mpi, servers);
			server.run();
		}
	}
	
	private static void printUsage() {
		System.err.println("Usage: java -jar LockService.jar <SERVER> <ID>");
		System.err.println("Usage: java -jar LockService.jar <CLIENT> <ID> <PORT>");
		System.exit(-1);
	}
}