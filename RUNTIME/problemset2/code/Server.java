import java.util.HashMap;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

public class Server implements Runnable {
	private final int id;
	private final MessagePassingInterface mpi;
	private final HashMap<Integer, ServerAddress> servers; 
	private final ServerAddress myAddress;
	
	private final SortedMap<Integer, PaxosServer> proposed; 
	private final SortedMap<Integer, Command> commited;
	private final SortedMap<Integer, Command> done;
	
	private final SortedMap<Integer, Result> results;
	
	private int currentPointer = 0; // point to the Paxos instance being processed in the queue
	private PaxosClient client;
	private final KVStore kv;
	
	private final Random rand = new Random();
	
	private int counter = 0;
	
	/***
	 * Constructor of a Server
	 * @param id
	 * @param mpi
	 * @param servers
	 */
	public Server(int id, MessagePassingInterface mpi, HashMap<Integer, ServerAddress> servers) {
		this.id = id;
		this.mpi = mpi;
		this.servers = servers;
		this.myAddress = servers.get(id);
		this.proposed = new TreeMap<Integer, PaxosServer>();
		this.commited = new TreeMap<Integer, Command>();
		this.done = new TreeMap<Integer, Command>();
		this.results = new TreeMap<Integer, Result> ();
		this.kv = new KVStore();
		this.client = null;
		this.currentPointer = 0;
	}
	
	
	/**
	 * Event-Driven Server Implementation
	 */
	@Override
	public void run() {
		while (true) {
			Message message;
			if (this.client == null) {
				message = mpi.getMessage(false);
			} else {
				message = mpi.getMessage(true);
				if (message == null) {
					Logger.printDebug("RESTART Server Recovery!");
					this.client.restart(new Command(currentPointer, myAddress, this.id, -1, 
							CommandType.NOP, null, 0));
					continue;
				}
			}
			
			Logger.printDebug(message.messageType.toString());
			
			if (message.messageType == MessageType.REQUESTSLOTID) {
				handleRequestSlotID(message);
				Logger.printDebug("HandleSLOTID!||||||||||||||||||||");
				continue;
			} else {
				Integer slotID = message.slotID;
				if (this.client != null) {
					if (message.messageType == MessageType.PAXOSPREPAREREPLY) {
						this.client.handleMessage(message);
						
						if (this.done.get(slotID) != null) {
							Logger.printDebug("Help Peer Know the Result!");
							Message m = new Message(slotID, this.id, message.clientSeq, myAddress, 
									MessageType.PAXOSACCEPTREPLY, this.done.get(slotID), 0);
							this.mpi.sendMessage(message.sender, m);
							continue;
						}
						
						continue;
					}
				}
				if (this.proposed.get(slotID) == null)
				{
					this.proposed.put(slotID, new PaxosServer(this, id, slotID, mpi, servers, myAddress));
				}
		
				
				this.proposed.get(slotID).handleMessage(message);
			}
			searchNextCommand();
		}
	}


	private void handleRequestSlotID(Message message) {
		int nextInstance = 0;
		if (this.proposed.size() > 0) {
			nextInstance = this.proposed.lastKey() + 1;
		}
		Message m = new Message(nextInstance, message.clientID, message.clientSeq, myAddress,
				MessageType.REQUESTSLOTIDREPLY, null, 0);
		this.mpi.sendMessage(message.sender, m);
	}
	
	private void searchNextCommand() {
		counter ++;
		if (this.commited.size() == 0) {
			return;
		}
		if (this.commited.firstKey() != currentPointer) {
			if (this.client != null) {
				if (counter > rand.nextInt(1000)) {
					Logger.printDebug("Server Recovery Restarts!");
					counter = 0;
					this.client = new PaxosClient(id, currentPointer, mpi, servers, myAddress);
					this.client.restart(new Command(currentPointer, myAddress, this.id, -1, 
							CommandType.NOP, null, 0));
				}
				return;
			}
			Logger.printDebug("Server Recovery Starts!");
			this.client = new PaxosClient(id, currentPointer, mpi, servers, myAddress);
			this.client.restart(new Command(currentPointer, myAddress, this.id, -1, 
					CommandType.NOP, null, 0));
			return;
		}
		this.client = null;
		this.counter = 0;
		while ((this.commited.size() > 0)&&(this.commited.firstKey() == currentPointer)) {
		Command command = this.commited.get(this.commited.firstKey());
			executeCommand(command);
			this.done.put(this.commited.firstKey(), command);
			this.commited.remove(this.commited.firstKey());
			currentPointer++;
		}
	}
	
	private void executeCommand(Command command) {
		Integer clientID = command.clientID;
		if ((this.results.containsKey(clientID))&&
		(command.clientSeq == this.results.get(clientID).clientSeq)) {
			Result result = this.results.get(clientID);
			if (command.commandType == CommandType.NOP) {
				Logger.printDebug("NOP is executed on server "+this.id);
			}
			if (command.commandType == CommandType.READ) {
				Message message = new Message(command.slotID, command.clientID, command.clientSeq, 
						myAddress, MessageType.READRESULT, command, result.result);
				this.mpi.sendMessage(command.commanderAddr, message);
				Logger.printDebug("READ is executed on server "+this.id); 
				Logger.printDebug("SlotID =  "+ command.slotID);
			}
			if (command.commandType == CommandType.WRITE) {
				Message message = new Message(command.slotID, command.clientID, command.clientSeq,
							myAddress, MessageType.WRITERESULT, command, 1);
				this.mpi.sendMessage(command.commanderAddr, message);
				Logger.printDebug("WRITE is executed on server "+this.id);
				Logger.printDebug("SlotID =  "+ command.slotID);

			}
			
			if (command.commandType == CommandType.LOCK) {
				Message message = new Message(command.slotID, command.clientID, command.clientSeq, 
							myAddress, MessageType.LOCKRESULT, command, 1);
				this.mpi.sendMessage(command.commanderAddr, message);
				Logger.printDebug("LOCK "+ command.variable +" from "+command.clientID+" is executed on server "+this.id);
				Logger.printDebug("SlotID =  "+ command.slotID);
			}
			
			if (command.commandType == CommandType.UNLOCK) {
				Message message = new Message(command.slotID, command.clientID, command.clientSeq, 
						myAddress, MessageType.UNLOCKRESULT, command, 0);
				this.mpi.sendMessage(command.commanderAddr, message);
				Logger.printDebug("UNLOCK is executed on server "+this.id);
				Logger.printDebug("SlotID =  "+ command.slotID);
			}
			return;
		}
		if (command.commandType == CommandType.NOP) {
			storeResult(command, 0);
			Logger.printDebug("NOP is executed on server "+this.id);
		}
		if (command.commandType == CommandType.READ) {
			int value = this.kv.read(command.variable);
			Message message = new Message(command.slotID, command.clientID, command.clientSeq, 
					myAddress, MessageType.READRESULT, command, value);
			this.mpi.sendMessage(command.commanderAddr, message);
			storeResult(command, value);
			Logger.printDebug("READ is executed on server "+this.id); 
			Logger.printDebug("SlotID =  "+ command.slotID);


		}
		if (command.commandType == CommandType.WRITE) {
			boolean flag = this.kv.write(command.clientID, command.variable, command.value);
			Message message = null;
			if (flag) {
				message = new Message(command.slotID, command.clientID, command.clientSeq, 
						myAddress, MessageType.WRITERESULT, command, 1);
				storeResult(command, 1);
			} else {
				message = new Message(command.slotID, command.clientID, command.clientSeq,
						myAddress, MessageType.WRITERESULT, command, -1);
			}
			this.mpi.sendMessage(command.commanderAddr, message);
			Logger.printDebug("WRITE is executed on server "+this.id);
			Logger.printDebug("SlotID =  "+ command.slotID);

		}
		
		if (command.commandType == CommandType.LOCK) {
			boolean flag = this.kv.lock(command.clientID, command.variable);
			Message message = null;
			if (flag) {
				message = new Message(command.slotID, command.clientID, command.clientSeq, 
						myAddress, MessageType.LOCKRESULT, command, 1);
				Logger.printDebug("LOCK "+ command.variable +" from "+command.clientID+" is executed on server "+this.id);
				storeResult(command, 1);
			} else {
				message = new Message(command.slotID, command.clientID, command.clientSeq,
						myAddress, MessageType.LOCKRESULT, command, -1);
			}
			this.mpi.sendMessage(command.commanderAddr, message);
			Logger.printDebug("LOCK is executed on server "+this.id);
			Logger.printDebug("SlotID =  "+ command.slotID);
		}
		
		if (command.commandType == CommandType.UNLOCK) {
			this.kv.unlock(command.variable);
			Message message = new Message(command.slotID, command.clientID, command.clientSeq, 
					myAddress, MessageType.UNLOCKRESULT, command, 0);
			this.mpi.sendMessage(command.commanderAddr, message);
			storeResult(command, 0);
			Logger.printDebug("UNLOCK is executed on server "+this.id);
			Logger.printDebug("SlotID =  "+ command.slotID);
		}
	}
	
	public void newCommitedTask(int slotID, Command command) {
		if (slotID < this.currentPointer) {
			return;
		}
		Integer s = slotID;
		this.commited.put(s, command);
		
		Logger.printDebug("I was notified that I can execute slotID:"+slotID);
	}
	
	private void storeResult(Command command, int result) {
		if (!this.results.containsKey(command.clientID)) {
			Integer clientID = command.clientID;
			this.results.put(clientID, new Result(clientID, command.clientSeq, result));
		}
	}
}
