import java.io.Serializable;

public class ServerAddress implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// it is just a data structure to store the hostname along with the port number
	public final String hostName;
	public final int port;
	
	public ServerAddress(String hostName, int port) {
		this.hostName = hostName;
		this.port = port;
	}
}
