
public interface MessagePassingInterface {
	public Message getMessage(boolean timeout);
	public void sendMessage(ServerAddress address, Message message);
	public void cleanUp();
}
